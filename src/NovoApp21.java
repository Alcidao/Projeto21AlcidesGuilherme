import java.util.Scanner;

public class NovoApp21 {
	public static void main(String[] args) {
		Baralho baralho = new Baralho();
    	Scanner scanner = new Scanner(System.in);

		System.out.println("***** Jogando 21 ****");

		baralho.preencherBaralho();
		baralho.embaralhar();

		Carta carta1 = baralho.retirarCarta();
		Carta carta2 = baralho.retirarCarta();

		baralho.macoMorto.add(carta1);
		baralho.macoMorto.add(carta2);


		// pegando 2 cartas do maço e mostrando para o jogador

		System.out.println("carta 1 = " + carta1);
		System.out.println("carta 2 = " + carta2);
		System.out.println("Voce quer mais cartas? Digite 'S' ou 'N' - Sua carta nesta rodada é: " + (carta1.getValorEfetivo() + carta2.getValorEfetivo()));

		String entrada  = scanner.next();

		if (entrada.equals("S")){
			while (!entrada.equals("N")) {
				entrada = entrada.intern();
				if (entrada.equals("S")) {

					Carta carta = baralho.retirarCarta();
					baralho.macoMorto.add(carta);

					System.out.println("Suas cartas na mesa = " + baralho.macoMorto + " Carta da rodada = " + carta);
					int resultado = 0;

					for (int i = 0; i < baralho.macoMorto.size(); i++) {
						resultado += baralho.macoMorto.get(i).getValorEfetivo();
					}

					System.out.println("Soma das suas cartas são: " + resultado);

					if(resultado == 21) {
						System.out.println("YOU WINNNN !!!");
					}else if(resultado > 21) {
						System.out.println("YOU LOOOSEEEE HAHAHA!!! ");
						return;
					}
					System.out.println("Voce quer mais cartas? Digite 'S' ou 'N'");
					entrada  = scanner.next();			    	
				}
			}
			System.out.println("Fim do Programa");

		}
		scanner.close();
	}
}
